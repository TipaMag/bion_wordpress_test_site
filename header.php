<?php


?>



<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>
	
	<header class="HomeIntro" style="background: url(<?php echo get_field('bg_image')?>) center center no-repeat;">
		<section class="HomeIntro-main">
			<a class="IntroLogo" href="#">
				<img class="IntroLogo-img" src="<?php echo get_field('logo_img')?>" alt="Logo">
			</a>
			<nav class="IntroNav">

				<?php    
					wp_nav_menu( array(
						'theme_location' => 'main_menu',
						'container' => 'ul',
					) ); 
				?>

			</nav>
			<a class="IntroPrice-btn" href="#">request price-list</a>
<!-- -burger -->
			<a href="#" class="nav-link"><span class="nav-line"></span></a>
            <nav class="menu-xl">
                <div class="navigation-close">
                    &times
                </div>
                <?php wp_nav_menu(array(
                'theme_location' => 'main_menu',
                'container' => 'ul',
                )); ?>
            </nav>
<!---burger -->
		</section>


		<?php
	            $query = new WP_Query(array(
	                'post_type' => 'intro'
	            ));
	    
	            while ($query->have_posts()) :
	                $query->the_post();
	        ?>
		<div class="HomeIntro-content" style="background-color:<?php echo get_field('intro_bg')?>;">
			<h1 class="IntroContent-title"><?php the_title(); ?></h1>
			<span class="IntroContent-info"><?php the_content(); ?></span>
		</div>
		<?php 
			endwhile;
			wp_reset_postdata();
		?>
	</header>
